#!/bin/bash
sudo apt-get update
sudo apt-get install -y build-essential cmake g++ gfortran git vim bzip2 gcc python3 python3-dev python3-pip
sudo apt-get clean
sudo apt-get autoremove
sudo rm -rf /var/lib/apt/lists/*
sudo pip3 --no-cache-dir install numpy setuptools
sudo pip3 --no-cache-dir install scipy matplotlib ipython jupyter np_utils fitsio seaborn
sudo pip3 --no-cache-dir install scikit-learn keras tensorflow
sudo pip3 --no-cache-dir install dist-keras 
# install from git master because bug with keras 1.0
sudo pip3 --no-cache-dir install git+git://github.com/maxpumperla/elephas
# downgrade flask because bug with notebook
sudo pip3 --no-cache-dir install flask==0.12.2

export SPARK_HOME="/opt/spark"
export PYSPARK_PYTHON=/usr/bin/python3
export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.9-src.zip:$PYTHONPATH
export PATH=$SPARK_HOME/bin:$PATH

echo 'export SPARK_HOME="/opt/spark"' >> ~/.bashrc
echo 'export PYSPARK_PYTHON=/usr/bin/python3' >> ~/.bashrc
echo 'export PYSPARK_PYTHON=/usr/bin/python3' >> /opt/spark/conf/spark-env.sh
echo 'export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.9-src.zip:$PYTHONPATH' >> ~/.bashrc
echo 'export PATH=$SPARK_HOME/bin:$PATH' >> ~/.bashrc

if [[ $HOSTNAME = *"master"* ]];
then
	for slave in `cat /opt/spark/conf/slaves`;
	do
		ssh $slave 'bash -s' < ${BASH_SOURCE[0]}
	done
	mkdir .jupyter
	echo "c.NotebookApp.ip = '*'" > .jupyter/jupyter_notebook_config.py
	echo "c.NotebookApp.port = 8888" >> .jupyter/jupyter_notebook_config.py
	echo "c.NotebookApp.open_browser = False" >> .jupyter/jupyter_notebook_config.py
	echo "c.MultiKernelManager.default_kernel_name = 'python3'" >> .jupyter/jupyter_notebook_config.py
fi
