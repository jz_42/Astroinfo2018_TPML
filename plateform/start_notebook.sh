export spark_master_hostname=193.55.95.175
SPARK_MASTER_IP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')

PYSPARK_PYTHON=/usr/bin/python3 PYSPARK_DRIVER_PYTHON=/usr/local/bin/jupyter PYSPARK_DRIVER_PYTHON_OPTS="notebook --no-browser --port=8888" pyspark --master spark://$SPARK_MASTER_IP:7077 --packages com.databricks:spark-csv_2.11:1.5.0
