sudo apt-get update
sudo apt-get install -y build-essential cmake g++ gfortran git vim bzip2 gcc
sudo apt-get -y clean
sudo apt-get -y autoremove
sudo rm -rf /var/lib/apt/lists/*

if [[ $HOSTNAME = *"master"* ]]; 
then
	wget http://apache.crihan.fr/dist/spark/spark-2.3.0/spark-2.3.0-bin-hadoop2.7.tgz
	wget http://apache.rediris.es/hadoop/common/hadoop-2.8.4/hadoop-2.8.4.tar.gz
	wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
	for slave in `cat /opt/spark/conf/slaves`;
        do
                scp spark-2.3.0-bin-hadoop2.7.tgz $slave:
                scp hadoop-2.8.4.tar.gz $slave:
                scp Miniconda3-latest-Linux-x86_64.sh $slave:
        done
fi

tar xvzf spark-2.3.0-bin-hadoop2.7.tgz
rm spark-2.3.0-bin-hadoop2.7.tgz

tar xvzf hadoop-2.8.4.tar.gz
rm hadoop-2.8.4.tar.gz

bash Miniconda3-latest-Linux-x86_64.sh -b -f
rm Miniconda3-latest-Linux-x86_64.sh

export CONDA_DIR=$HOME/miniconda3
unset PYTHON_LIB_DIR
unset PYTHON_SITE_DIR
unset PYTHONPATH
export PATH=$CONDA_DIR/bin:$PATH
source $CONDA_DIR/etc/profile.d/conda.sh
export HADOOP_HOME="$HOME/hadoop-2.8.4"
export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native:$LD_LIBRARY_PATH
export SPARK_HOME="$HOME/spark-2.3.0-bin-hadoop2.7/"
export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.6-src.zip:$PYTHONPATH
export PATH=$SPARK_HOME/bin:$PATH
export PYSPARK_PYTHON=$HOME/miniconda3/bin/python

export SPARK_HOME="$HOME/spark-2.3.0-bin-hadoop2.7/"
export IPYTHON=1
export PYSPARK_PYTHON=/usr/bin/python3
export PYSPARK_DRIVER_PYTHON=ipython3
export PYSPARK_DRIVER_PYTHON_OPTS="notebook"

echo '' >> ~/.bashrc
echo 'export CONDA_DIR=$HOME/miniconda3' >> ~/.bashrc
echo 'unset PYTHON_LIB_DIR' >> ~/.bashrc
echo 'unset PYTHON_SITE_DIR' >> ~/.bashrc
echo 'unset PYTHONPATH' >> ~/.bashrc
echo 'export PATH=$CONDA_DIR/bin:$PATH' >> ~/.bashrc
echo 'source $CONDA_DIR/etc/profile.d/conda.sh' >> ~/.bashrc
echo '' >> ~/.bashrc
echo '# Hadoop' >> ~/.bashrc
echo 'export HADOOP_HOME="$HOME/hadoop-2.8.4"' >> ~/.bashrc
echo 'export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native:$LD_LIBRARY_PATH' >> ~/.bashrc
echo '' >> ~/.bashrc
echo '# Spark' >> ~/.bashrc
echo 'export SPARK_HOME="$HOME/spark-2.3.0-bin-hadoop2.7/"' >> ~/.bashrc
echo 'export PYTHONPATH=$SPARK_HOME/python:$SPARK_HOME/python/lib/py4j-0.10.6-src.zip:$PYTHONPATH' >> ~/.bashrc
echo 'export PYSPARK_PYTHON=$HOME/miniconda3/bin/python' >> ~/.bashrc
echo 'export PATH=$SPARK_HOME/bin:$PATH' >> ~/.bashrc
echo '' >> ~/.bashrc

conda update -y -n base conda
conda update --all -y
conda install -y gcc_linux-64 gfortran_linux-64 gxx_linux-64 numpy
conda install -y scipy matplotlib ipython jupyter scikit-learn keras tensorflow cython seaborn 
conda clean -y --all
pip --no-cache-dir install fitsio dist-keras spark-sklearn

# run the script on slave
if [[ $HOSTNAME = *"master"* ]];
then
	for slave in `cat /opt/spark/conf/slaves`;
	do
		ssh $slave 'bash -s' < ${BASH_SOURCE[0]}
	done

	# stop old spark
	/opt/spark/sbin/stop-slaves.sh
	/opt/spark/sbin/stop-master.sh
	
	# start spark
	SPARK_MASTER_IP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1') 
	cp /opt/spark/conf/slaves $HOME/spark-2.3.0-bin-hadoop2.7/conf/slaves
	echo SPARK_MASTER_IP=$SPARK_MASTER_IP > $HOME/spark-2.3.0-bin-hadoop2.7/conf/spark-env.sh
	echo export PYSPARK_PYTHON=$HOME/miniconda3/bin/python >> $HOME/spark-2.3.0-bin-hadoop2.7/conf/spark-env.sh
	# copy conf files to slave, is it usefull?
	for slave in `cat $HOME/spark-2.3.0-bin-hadoop2.7/conf/slaves`;
	do
		scp $HOME/spark-2.3.0-bin-hadoop2.7/conf/slaves $slave:$HOME/spark-2.3.0-bin-hadoop2.7/conf/slaves
		scp $HOME/spark-2.3.0-bin-hadoop2.7/conf/spark-env.sh $slave:$HOME/spark-2.3.0-bin-hadoop2.7/conf/spark-env.sh
	done
	
	# master
	$SPARK_HOME/sbin/start-master.sh -h $SPARK_MASTER_IP
	$SPARK_HOME/sbin/start-slaves.sh
	
	# setup jupyter server config
	mkdir .jupyter
	echo "c.NotebookApp.ip = '*'" > .jupyter/jupyter_notebook_config.py
	echo "c.NotebookApp.port = 8888" >> .jupyter/jupyter_notebook_config.py
	echo "c.NotebookApp.open_browser = False" >> .jupyter/jupyter_notebook_config.py
	echo "c.MultiKernelManager.default_kernel_name = 'python3'" >> .jupyter/jupyter_notebook_config.py
fi
